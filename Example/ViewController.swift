//
//  ViewController.swift
//  Example
//
//  Created by Алексей Гладков on 25.02.18.
//  Copyright © 2018 Alex Gladkov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        var objectArray = [MaterialObject]()
        
        // Do any additional setup after loading the view, typically from a nib.
        let bird1 = Bird()
        let bird2 = YellowBird()
        let bird3 = PerepelkaBird()
        let bird4 = WoodenBird()
        let plane = Plane()
        let box = Box()
        let pegasus = Pegasus()
        
        objectArray.append(bird1)
        objectArray.append(bird2)
        objectArray.append(bird3)
        objectArray.append(bird4)
        objectArray.append(plane)
        objectArray.append(box)
        objectArray.append(pegasus)
        
    
        objectArray.forEach { (obj) in
            obj.name()
            
            if (obj is Flyable) {
                (obj as! Flyable).fly()
            }
        }
        
//        bird1.name()
//        bird1.fly()
//        print("\(bird1)")
//
//        bird2.name()
//        bird2.fly()
//         print("\(bird2)")
//
//        bird3.name()
//        bird3.fly()
//         print("\(bird3)")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


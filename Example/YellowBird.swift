//
//  YellowBird.swift
//  Example
//
//  Created by Алексей Гладков on 25.02.18.
//  Copyright © 2018 Alex Gladkov. All rights reserved.
//

import Foundation

class YellowBird: Bird, Flyable {

    override func name() {
        print("I'm yellow bird")
    }
    
    func fly() {
        print("I'm flying fast")
    }
    
    func paint() {
        print("I'm painting")
    }
}

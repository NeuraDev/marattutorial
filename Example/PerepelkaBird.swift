//
//  PerepelkaBird.swift
//  Example
//
//  Created by Алексей Гладков on 25.02.18.
//  Copyright © 2018 Alex Gladkov. All rights reserved.
//

import Foundation

class PerepelkaBird: Bird, Flyable {
    
    func fly() {
        print("I'm flying far")
    }
    
    override func name() {
        print("I'm perepelka bird")
    }
    
    func run() {
        print("I'm running")
    }
}

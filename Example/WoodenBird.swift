//
//  WoodenBird.swift
//  Example
//
//  Created by Алексей Гладков on 25.02.18.
//  Copyright © 2018 Alex Gladkov. All rights reserved.
//

import Foundation

class WoodenBird: Bird {
    
    override func name() {
        print("I'm wooden bird")
    }
}

//
//  Plane.swift
//  Example
//
//  Created by Алексей Гладков on 25.02.18.
//  Copyright © 2018 Alex Gladkov. All rights reserved.
//

import Foundation

class Plane: MaterialObject, Flyable {
    
    override func name() {
        print("I'm plane")
    }
    
    func fly() {
        print("I'm flying for money")
    }
}
